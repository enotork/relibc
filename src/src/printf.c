#include <stdarg.h>						// Function arguments handling

#include "stdio.h"

/** @fn static uint8_t print_int( uint32_t val, int8_t base, uint8_t npads )
 *  @brief Print an integer data value in various forms to buffer.
 *  @param val   : Integer data value to print.
 *  @param base  : Base notation to print. Can be one of the following values:
 *      @arg  10 : Unsigned decimal.
 *      @arg -10 : Signed decimal.
 *      @arg  16 : Unsigned hexadecimal.
 *      @arg -16 : Signed hexadecimal.
 *  @param npads : Number of padding characters to print for correct printing alignment.
 *  @return Total number of characters printed.
 */
static uint8_t print_int( uint32_t val, int8_t base, uint8_t npads ) {
	char str[12];	// 32-bit = 10 character + 1 for minus
	uint8_t count = 0;
	uint8_t i = 0, j = 0;
	uint8_t neg = 0;
	int32_t ival;
	char ch;
	
	// Negative base mean signed integer
	if( base < 0 ) {
		// Convert unsigned to signed
		base = -base;
		ival = ( int32_t )val;
		// If negative number, convert to positive and remember it
		if( ival < 0 ) {
			val = -ival;
			neg = 1;
		}
	}
	
	// Loop until we can divide value
	do {
		// Get remainder of a division of value by base, this is our character to print
		ch = ( char )( ( uint32_t )val % base );
		// Negative hexadecimal? Then calculate one's complement
		if( base == 16 && neg == 1 )
			// LSB? then two's complement
			ch = ( i == 0 ) ? 0x10 - ch : 0xF - ch;
		// Convert value into ASCII character
		ch += ( ch >= 10 ) ? ( 'A' - 10 ) : '0'; // ( ch > 10 ) ? ABCDEF : 0123456789
		// Put character inside string
		str[i++] = ch;
		// Divide by base and go on
		val = ( uint32_t )val / base;
	}
	while( val > 0 );
	
	// Decimal negative? Then include the minus sign before padding
	if( base == 10 && neg == 1 )
		str[i++] = '-';
	
	// Decimal? Space padding
	if( base == 10 )
		ch = ' ';
	// Hexadecimal positive? Zero padding
	else if( base == 16 && neg == 0 )
		ch = '0';
	// Hexadecimal negative? FFs padding
	else if( base == 16 && neg == 1 )
		ch = 'F';
	// Padding
	for( j = i; j < npads; j++ ) {
		putchar( ch );
		count ++;
	}
	
	// Send characters of number
	while( i > 0 ) {
		putchar( str[--i] );
		count ++;
	}
	return count;
}

void putchar( char ch ) {
	ch = '\0';
}

uint16_t printf( const char* format, ... ) {
	va_list args;
	const char* pFor = format;
	uint16_t count = 0;
	int8_t size = 32;
	uint8_t npads = 0;
	uint32_t val;
	char* pStr;
	
	// Start args handles
	va_start( args, format );
	
	// Loop until end of format string
	while( *pFor != '\0' ) {
		// Specifier?
		if( *pFor == '%' ) {
			pFor++;
			
			// Padding
			while( *pFor >= '0' && *pFor <= '9' )
				npads = npads * 10 + *pFor++ - '0' ;
			
			// Shorter size (min 8 bit)
			while( *pFor == 'h' && size > 8 ) {
				size /= 2;
				pFor++;
			}
			// Longer size (max 32-bit)
			while( *pFor == 'l' && size < 32 ) {
				size *= 32;
				pFor++;
			}
			
			// Check specifier type
			switch( *pFor ) {
				case 'i':	// Signed integer
					// Print number
					if( size == 8 )
						val = ( int8_t )va_arg( args, int32_t );
					else if( size == 16 )
						val = ( int16_t )va_arg( args, int32_t );
					else
						val = ( int32_t )va_arg( args, int32_t );
					count += print_int( val, -10, npads );
					break;
					
				case 'u':	// Unsigned integer
					// Print number
					if( size == 8 )
						val = ( uint8_t )va_arg( args, uint32_t );
					else if( size == 16 )
						val = ( uint16_t )va_arg( args, uint32_t );
					else
						val = ( uint32_t )va_arg( args, uint32_t );
					count += print_int( val, 10, npads );
					break;
				case  'x':	// Hexadecimal
					// Print number
					if( size == 8 )
						val = ( int8_t )va_arg( args, int32_t );
					else if( size == 16 )
						val = ( int16_t )va_arg( args, int32_t );
					else
						val = ( int32_t )va_arg( args, int32_t );
					count += print_int( val, -16, npads );
					break;
				case '%':	// Print '%'
					putchar( '%' );
					count++;
					break;
				case  'c':	// Character
					putchar( va_arg( args, int ) );
					count++;
					break;
				case 's':	// String
					pStr = va_arg( args, char* );
					// Send string
					while( *pStr != '\0' ) {
						putchar( *pStr++ );
						count++;
					}
					break;
				default:	// Error in format string, invalid specifier
					return count;
			}
			// If we are here, then a specifier is parsed correctly
			size = 32;	// Restore 32-bit size
			npads = 0;	// Restore no-padding
			pFor++;
		}
		else {
			// Not a specifier character
			putchar( *pFor++ );
			count++;
		}
	}
	
	// End args handles
	va_end( args );
	return count;
}
