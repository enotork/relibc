#include <stdarg.h>						// Function arguments handling

#include "stdio.h"

/** @fn static uint8_t scan_int( int32_t* val, uint8_t base )
 *  @brief Scan for an integer data value in various forms from buffer.
 *  @param val   : Pointer to the destination storage integer variable.
 *  @param base  : Base notation to scan. Can be one of the following values:
 *      @arg  10 : Decimal.
 *      @arg  16 : Hexadecimal.
 *  @return Total number of characters scanned.
 */
static uint8_t scan_int( int32_t* val, uint8_t base ) {
	uint8_t count = 0;
	char ch;
	uint8_t neg = 0;
	int32_t value = 0;
	uint8_t tmp;
	
	// Get first character
	ch = getchar();
	// Decimal with minus? Then it's negative
	if( base == 10 && ch == '-' ) {
		neg = 1;
		ch = getchar();
	}
	
	// Loop until we can receive numbers characters
	do {
		// Is a [0-9] character?
		if( ch >= '0' && ch <= '9' )
			tmp =  ch - '0' ;
		// Is a [a-f] character?
		else if( base == 16 && ch >= 'a' && ch <= 'f' )
			tmp =  ch - 'a' + 10 ;
		// Is a [A-F] character?
		else if( base == 16 && ch >= 'A' && ch <= 'F' )
			tmp =  ch - 'A' + 10 ;
		else
			// Not a number, end parsing
			break;
		// Multiply accumulator by base and add character
		value = value * base + tmp;
		count++;
	}
	while( ( ch = getchar() ) );
	
	// Return value
	*val = ( neg == 1 ) ? -value : value;
	return count;
}

char getchar( void ) {
	return '\0';
}

uint16_t scanf( const char* format, ... ) {
	va_list args;
	const char* pFor = format;
	uint16_t count = 0;
	uint8_t length = 0;
	char* pStr;
	char ch;
	
	// Start args handles
	va_start( args, format );
	
	// Loop until end of format string
	while( *pFor != '\0' ) {
		// Bypass spaces in format string
		while( *pFor == ' ' )
			pFor++;
		
		// Specifier?
		if( *pFor == '%' ) {
			pFor++;
			
			// Length
			while( *pFor >= '0' && *pFor <= '9' )
				length = length * 10 + *pFor++ - '0' ;
			
			// Check specifier type
			switch( *pFor ) {
				case 'd':	// Decimal notation
					count += scan_int( va_arg( args, int32_t* ) , 10 );
					break;
				case 'x':	// Hexadecimal notation
					count += scan_int( va_arg( args, int32_t* ) , 16 );
					break;
				case '%':	// Scan '%'
					if( getchar() != '%' )
						return count;
					break;
				case 'c':	// Single character
					*( va_arg( args, char* ) ) = getchar();
					break;
				case 's':	// String
					// If no length specified, then use maximum length available
					if( length == 0 )
						length--;
					// Get string pointer
					pStr = va_arg( args, char* );
					do {
						// Get character
						ch = getchar();
						// Is a white space? then exit
						if( ch == ' ' || ch == '\t' || ch == '\n' || ch == '\v' || ch == '\f' || ch == '\r' )
							break;
						// Add character to string
						*pStr++ = ch;
						count++;
						length--;
					}
					while( length > 0 );
					// End string
					*pStr = '\0';
					break;
				default:	// Error in format string, invalid specifier
					return count;
			}
			// If we are here, then a specifier is parsed correctly
			length = 0;
			pFor++;
		}
		// Standard matching character
		else if( *pFor == getchar() )
			pFor++;
		// Standard non-matching character
		else
			return count;
	}
	
	// End args handles
	va_end( args );
	return count;
}
