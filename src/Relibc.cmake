################################################################################
### Relibc: libc reduced version for embedded environment.
#
# When included:
#	Find relibc package and include headers directory.
#
# Defines following public functions:
#	relibc_add_target			: Set include directory for override default libc and link to target.



################################################################################
### Relibc.cmake

### Find Relibc library
find_package("Relibc" 1.0 REQUIRED)
if(DEFINED Relibc_FOUND)
	message(STATUS "RElibC: Founded library v.${Relibc_VERSION}.")
else()
	message(FATAL_ERROR "RElibC: Can't find RElibC library on your system.")
endif()

### Include Relibc library headers
include_directories(SYSTEM ${Relibc_INCLUDE_DIR})

### Search library file for this chip type
find_library(RELIBC "librelibc.a" PATHS ${Relibc_LIB_DIR} NO_DEFAULT_PATH)
if(NOT RELIBC)
	message(FATAL_ERROR "RElibC: Can't find RElibC file 'librelibc.a'.")
endif()



################################################################################
### relibc_add_target
# Link relibc library to target TARGET.
#
# Args:
#	TARGET	: Name of the main project's target.
function(relibc_add_target TARGET)
		### Link library
		target_link_libraries(${TARGET} ${RELIBC})
		message(STATUS "RElibC: library linked to target '${TARGET}'")
endfunction()
