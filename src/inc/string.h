/**
 * *****************************************************************************
 * @file	string.h
 * @author	Ivan Pessotto
 * @version	v.1.0.0
 * @date	10-June-2015
 * @brief	String manipulation helpers.
 * *****************************************************************************
 */

#ifndef _STRING_H
#define _STRING_H

#include <stdint.h>						// Standard types

/** @defgroup string_h string.h
 *  @{
 *      @defgroup string_h_Functions Functions
 *      @{
 */

/** @fn int8_t strcmp( const char* str1, const char* str2 )
 *  @brief Compare string pointed to by str1 to the string pointed to str2.
 *  @param str1 : First string to compare.
 *  @param str2 : Second string to compare.
 *  @retval ==0 : If str1 is equal to str2.
 *  @retval  >0 :  If str1 is greather than str2.
 *  @retval  <0 :  If str1 is less than str2.
 *  @note Calling strcmp( "", "" ) add about 52 bytes of size.
 */
int8_t strcmp( const char* str1, const char* str2 );

/**      @} string_h
 *  @} string_h_Functions
 */

#endif // _STRING_H
