/**
 * *****************************************************************************
 * @file	stdio.h
 * @author	Ivan Pessotto
 * @version	v.1.0.0
 * @date	10-June-2015
 * @brief	Standard input/output helpers.
 * *****************************************************************************
 */

#ifndef _STDIO_H
#define _STDIO_H

#include <stdint.h>						// Standard types

/** @defgroup stdio_h stdio.h
 *  @{
 *      @defgroup stdio_h_Functions Functions
 *      @{
 */

/** @fn void putchar( char ch )
 *  @brief Dummy function. Put a char to stdout.
 *  @param ch : Character to send.
 *  @note Must be overridden for printf to work (like USART).
 */
void putchar( char ch )__attribute__( ( weak ) );

/** @fn char getchar( void )
 *  @brief Dummy function. Get a char from stdin.
 *  @return Character received.
 *  @note Must be overridden for scanf to work (like USART).
 */
char getchar( void )__attribute__( ( weak ) );

/** @fn void printf( const char* format, ... )
 *  @brief Reduced printf function for use in embedded environment.
 *  @param format : Formatted string to print with specifiers:
 *      @arg %hhi    : Decimal integer signed 8-bit
 *      @arg %hhu    : Decimal integer unsigned 8-bit
 *      @arg %hi     : Decimal integer signed 16-bit
 *      @arg %hu     : Decimal integer unsigned 16-bit
 *      @arg %i      : Decimal integer signed 32-bit
 *      @arg %u      : Decimal integer unsigned 32-bit
 *      @arg %x      : Hexadecimal integer signed
 *      @arg %%      : '%' character
 *      @arg %c      : Char
 *      @arg %s      : String (char*)
 *      @arg %__attribute__ ((weak))[0-9]# : A number preceding one of above number specifiers.
 *  @param  ... : Optional parameters to match with specifiers in format string.
 *  @return Total number of characters printed.
 *  @note Override putchar() function for a working printf.
 *  @note Calling printf( "" ) add about 716 bytes of size.
 */
uint16_t printf( const char* format, ... );

/** @fn void scanf( const char* format, ... )
 *  @brief Reduced scanf function for use in embedded environment.
 *  @param format : Formatted string to scan with specifiers:
 *      @arg %d : Decimal integer signed and unsigned
 *      @arg %x : Hexadecimal integer signed or unsigned
 *      @arg %c : Char
 *      @arg %s : String (char*)
 *  @param  ... : Optional parameters to match with specifiers in format sting.
 *  @return Total number of characters scanned.
 *  @note Override getchar() function for a working scanf.
 *  @note Calling scanf( "" ) add about 580 bytes of size.
 */
uint16_t scanf( const char* format, ... );

/**     @} stdio_h_Functions
 *  @} stdio_h
 */

#endif // _STDIO_H
