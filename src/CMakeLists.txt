##### Variables ################################################################
file(GLOB H_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "inc/*.h")
file(GLOB C_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "src/*.c")

### Destination installation directories
set(INCLUDE_INSTALL_DIR "include")
set(LIB_INSTALL_DIR "lib")
set(CONFIG_INSTALL_DIR "lib/cmake/${PROJECT_NAME}")
set(MODULES_INSTALL_DIR "share/cmake/Modules")

##### Includes #################################################################
include_directories("inc")

##### Building #################################################################
add_library(${MAIN_TARGET} STATIC ${C_FILES})

### Include package config helpers
include(CMakePackageConfigHelpers)

### Build 'Config.cmake' and 'ConfigVersion.cmake' helpers
configure_package_config_file("Config.cmake.in" "${PROJECT_NAME}Config.cmake"
	INSTALL_DESTINATION ${CONFIG_INSTALL_DIR}
	PATH_VARS INCLUDE_INSTALL_DIR LIB_INSTALL_DIR
	NO_CHECK_REQUIRED_COMPONENTS_MACRO)

write_basic_package_version_file("${PROJECT_NAME}ConfigVersion.cmake"
	COMPATIBILITY AnyNewerVersion)

##### Installing ###############################################################
install(FILES ${H_FILES} DESTINATION ${INCLUDE_INSTALL_DIR})
install(TARGETS ${MAIN_TARGET} ARCHIVE DESTINATION ${LIB_INSTALL_DIR})
install(FILES
	"${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
	"${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
	DESTINATION ${CONFIG_INSTALL_DIR})
install(FILES
	"Relibc.cmake"
	DESTINATION ${MODULES_INSTALL_DIR})
